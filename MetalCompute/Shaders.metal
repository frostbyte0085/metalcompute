//
//  Shaders.metal
//  MetalCompute
//
//  Created by Pantelis Lekakis on 24/08/2016.
//  Copyright (c) 2016 Pantelis Lekakis. All rights reserved.
//

#include <metal_stdlib>
#include <simd/simd.h>
#include "SharedStructures.h"

using namespace metal;

// Variables in constant address space
constant float3 light_position = float3(0.0, 1.0, -1.0);
constant float4 ambient_color  = float4(0.18, 0.24, 0.8, 1.0);
constant float4 diffuse_color  = float4(0.4, 0.4, 1.0, 1.0);

typedef struct
{
    float3 position [[attribute(0)]];
    float3 normal [[attribute(1)]];
} vertex_t;

typedef struct {
    float4 position [[position]];
    half4  color;
} ColorInOut;

// Vertex shader function
vertex ColorInOut lighting_vertex(vertex_t vertex_array [[stage_in]],
                                  constant uniforms_t& uniforms [[ buffer(1) ]])
{
    ColorInOut out;
    
    float4 in_position = float4(vertex_array.position, 1.0);
    out.position = uniforms.modelview_projection_matrix * in_position;
    
    float4 eye_normal = normalize(uniforms.normal_matrix * float4(vertex_array.normal, 0.0));
    float n_dot_l = dot(eye_normal.rgb, normalize(light_position));
    n_dot_l = fmax(0.0, n_dot_l);
    
    out.color = half4(ambient_color + diffuse_color * n_dot_l);
    return out;
}

// Fragment shader function
fragment half4 lighting_fragment(ColorInOut in [[stage_in]])
{
    return in.color;
}

inline float luminance(float3 v) {
    const float3 w = float3(0.2125, 0.7154, 0.0721);
    return dot(v, w);
}

kernel void invert(texture2d<float, access::read> framebuffer[[texture(0)]],
                    texture2d<float, access::write> output[[texture(1)]],
                    uint2 id [[ thread_position_in_grid ]]) {
    
    float4 f = framebuffer.read(id);
    f.rgb = 1.0 - f.rgb;
    output.write(f, id);
}

kernel void scanlines(texture2d<float, access::read> framebuffer[[texture(0)]],
                   texture2d<float, access::write> output[[texture(1)]],
                   uint2 id [[ thread_position_in_grid ]]) {
    
    float4 f = framebuffer.read(id);
    output.write(f * (id.y % 4 == 0), id);
}

kernel void greyscale(texture2d<float, access::read> framebuffer[[texture(0)]],
                      texture2d<float, access::write> output[[texture(1)]],
                      uint2 id [[thread_position_in_grid]]) {
    
    float4 f = framebuffer.read(id);
    f.rgb = luminance(f.rgb);
    output.write ( f, id );
}

kernel void blur(texture2d<float, access::read> framebuffer[[texture(0)]],
                      texture2d<float, access::write> output[[texture(1)]],
                      constant blur_t& uniforms [[ buffer(2) ]],
                      uint2 id [[thread_position_in_grid]]) {
    
    const uint offset[5] = { 0, 1, 2, 3, 4 };
    const float weight[5] = { 0.2270270270, 0.1945945946, 0.1216216216, 0.0540540541, 0.0162162162 };
    
    float4 color = framebuffer.read(id) * weight[0];
    
    for (int i=1; i<5; i++) {
        color += framebuffer.read(id + offset[i] * (uint2)uniforms.mask ) * weight[i];
        color += framebuffer.read(id - offset[i] * (uint2)uniforms.mask ) * weight[i];
    }
    
    output.write ( color, id );
}