//
//  main.m
//  MetalCompute
//
//  Created by Pantelis Lekakis on 24/08/2016.
//  Copyright © 2016 Pantelis Lekakis. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
