//
//  AppDelegate.h
//  MetalCompute
//
//  Created by Pantelis Lekakis on 24/08/2016.
//  Copyright (c) 2016 Pantelis Lekakis. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end