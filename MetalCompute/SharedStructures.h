//
//  SharedStructures.h
//  MetalCompute
//
//  Created by Pantelis Lekakis on 24/08/2016.
//  Copyright (c) 2016 Pantelis Lekakis. All rights reserved.
//

#ifndef SharedStructures_h
#define SharedStructures_h

#include <simd/simd.h>

typedef struct __attribute__((__aligned__(256)))
{
    matrix_float4x4 modelview_projection_matrix;
    matrix_float4x4 normal_matrix;
} uniforms_t;

typedef struct __attribute__((__aligned__(256)))
{
    vector_int2 mask;
} blur_t;

#endif /* SharedStructures_h */

