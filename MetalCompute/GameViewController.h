//
//  GameViewController.h
//  MetalCompute
//
//  Created by Pantelis Lekakis on 24/08/2016.
//  Copyright (c) 2016 Pantelis Lekakis. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <MetalKit/MTKView.h>

@interface GameViewController : NSViewController<MTKViewDelegate>


@end